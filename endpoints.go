package vendorsvc

import (
	"net/url"
	"strings"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

type Endpoints struct {
	GetVendorsEndpoint endpoint.Endpoint
}

func MakeServerEndpoints(s Service) Endpoints {
	return Endpoints{
		GetVendorsEndpoint: MakeGetVendorsEndpoint(s),
	}
}

func MakeClientEndpoints(instance string) (Endpoints, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	tgt, err := url.Parse(instance)
	if err != nil {
		return Endpoints{}, err
	}
	tgt.Path = ""

	options := []httptransport.ClientOption{}

	return Endpoints{
		GetVendorsEndpoint: httptransport.NewClient("POST", tgt, encodeGetVendorsRequest, decodeGetVendorsResponse, options...).Endpoint(),
	}, nil
}

func MakeGetVendorsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getVendorsRequest)
		e := s.GetVendors(ctx)
		r := ""
		if req.Debug == "1" && e != nil {
			r = e.Error()
		}
		ResponseTime = time.Now().UTC().String()
		return getVendorsResponse{
			ResponseTime:    ResponseTime,
			ResponseStatus:  codeFrom(e),
			ResponseMessage: r,
			Err:             e,
		}, nil
	}
}

func (e Endpoints) GetVendors(ctx context.Context) error {
	request := getVendorsRequest{Debug: Debug}
	response, err := e.GetVendorsEndpoint(ctx, request)
	if err != nil {
		return err
	}
	resp := response.(getVendorsResponse)
	return resp.Err
}

// GetVendors
type getVendorsRequest struct {
	Debug string
}

type getVendorsResponse struct {
	ResponseTime    string `json:"response_time"`
	ResponseStatus  int    `json:"response_status"`
	ResponseMessage string `json:"response_msg,omitempty"`
	Err             error  `json:"err,omitempty"`
}

func (r getVendorsResponse) error() error {
	return r.Err
}

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/net/context"

	vendorsvc "svc-sourcingtool-api-vendor"

	"github.com/go-kit/kit/log"
)

func main() {

	fmt.Println("svc-sourcingtool-api-vendor")

	var (
		httpAddr = flag.String("http.addr", ":"+os.Getenv("SOURCING_VENDOR_API_PORT"), "HTTP listen address")
	)
	flag.Parse()

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewContext(logger).With("ts", log.DefaultTimestampUTC)
		logger = log.NewContext(logger).With("caller", log.DefaultCaller)
	}

	var ctx context.Context
	{
		ctx = context.Background()
	}

	var s vendorsvc.Service
	{
		s = vendorsvc.NewVendorService()
		s = vendorsvc.LoggingMiddleware(logger)(s)
	}

	var h http.Handler
	{
		h = vendorsvc.MakeHTTPHandler(ctx, s, log.NewContext(logger).With("component", "HTTP"))
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		logger.Log("transport", "HTTP", "addr", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, h)
	}()

	logger.Log("exit", <-errs)
}

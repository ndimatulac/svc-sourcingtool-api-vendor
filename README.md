# README #

Sourcing Tool - API Vendor

REST API for Vendor written in Go
Version X.X

### Requirements ###
- docker installed
- mysql database
- fill up config file (.env)

### How to install on dev instance? ###
Execute the ff on cmd line
``` bash
docker run --rm --privileged -it --name svc-sourcingtool-api-vendor -v $PWD:/go/src/svc-sourcingtool-api-vendor -v $HOME/.ssh:/root/.ssh -w /go/src/svc-sourcingtool-api-vendor --env-file .env -p 8089:8089 billyteves/alpine-golang-glide:1.1.1-test bash
run-ssh;
make;
./bin/svc-sourcingtool-api-vendor;
```

API URL - http://localhost:8089/

### Functions ###

Service: GetVendors
- GET - http://localhost:8089/SourcingTool/Vendors/
Request:
```json
{
	"debug": 1,
}
```
Response
```json
{
  "response_time": "2017-02-16 05:47:01.363556996 +0000 UTC",
  "response_status": 200
}
```
.PHONY: build run test clean vendor

default: build

build: test 
	go build -o ./bin/svc-sourcingtool-api-vendor cmd/vendorsvc/main.go

run:
	go run main.go

test: vendor

clean:
	rm -Rf ${PWD}/vendor ${PWD}/bin

vendor: clean
	glide cc
	glide install

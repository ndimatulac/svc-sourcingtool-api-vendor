package vendorsvc

// The profilesvc is just over HTTP, so we just have a single transport.go.

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"net/url"

	"github.com/gorilla/mux"
	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
)

var (
	// ErrBadRouting is returned when an expected path variable is missing.
	// It always indicates programmer error.
	ErrBadRouting = errors.New("inconsistent mapping between route and handler (programmer error)")
	// ErrUnableToParse is returned when an error occurred while parsing URI params
	ErrUnableToParse = errors.New("Unable to parse query string")
	// ResponseTime is the response time
	ResponseTime = ""
	// Debug is a switch for debug
	Debug = "0"
)

// MakeHTTPHandler mounts all of the service endpoints into an http.Handler.
// Useful in a profilesvc server.
func MakeHTTPHandler(ctx context.Context, s Service, logger log.Logger) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(s)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(encodeError),
	}

	r.Methods("POST").Path("/SourcingTool/Vendors/").Handler(httptransport.NewServer(
		ctx,
		e.GetVendorsEndpoint,
		decodeGetVendorsRequest,
		encodeResponse,
		options...,
	))
	return r
}

//=============== Decode Request ===================

func decodeGetVendorsRequest(_ context.Context, r *http.Request) (request interface{}, err error) {

	path := r.RequestURI
	u, err := url.Parse(path)
	if err != nil {
		return nil, err
	}
	m, err := url.ParseQuery(u.RawQuery)
	if err != nil {
		return getVendorsRequest{}, ErrUnableToParse
	}
	debug, ok := m["debug"]
	if !ok {
		debug = []string{"0"}
	}
	Debug = debug[0]

	return getVendorsRequest{Debug: debug[0]}, nil
}

func encodeGetVendorsRequest(ctx context.Context, req *http.Request, request interface{}) error {
	req.Method, req.URL.Path = "POST", "/SourcingTool/Vendors/"
	return encodeRequest(ctx, req, request)
}

func decodeGetVendorsResponse(_ context.Context, resp *http.Response) (interface{}, error) {
	var response getVendorsResponse
	err := json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}

// errorer is implemented by all concrete response types that may contain
// errors. It allows us to change the HTTP response code without needing to
// trigger an endpoint (transport-level) error. For more information, read the
// big comment in endpoints.go.
type errorer interface {
	error() error
}

// encodeResponse is the common method to encode all response types to the
// client. I chose to do it this way because, since we're using JSON, there's no
// reason to provide anything more specific. It's certainly possible to
// specialize on a per-response (per-method) basis.
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// encodeRequest likewise JSON-encodes the request to the HTTP request body.
// Don't use it directly as a transport/http.Client EncodeRequestFunc:
// profilesvc endpoints require mutating the HTTP method and request path.
func encodeRequest(_ context.Context, req *http.Request, request interface{}) error {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(request)
	if err != nil {
		return err
	}
	req.Body = ioutil.NopCloser(&buf)
	return nil
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	errMsg := err.Error()
	if Debug == "0" {
		errMsg = ""
	}
	json.NewEncoder(w).Encode(errorResponse{
		ResponseTime:    ResponseTime,
		ResponseStatus:  codeFrom(err),
		ResponseMessage: errMsg,
		Result:          map[string]interface{}{},
	})
}

type errorResponse struct {
	ResponseTime    string      `json:"response_time"`
	ResponseStatus  int         `json:"response_status"`
	ResponseMessage string      `json:"response_message,omitempty"`
	Result          interface{} `json:"result"`
}

func codeFrom(err error) int {
	switch err {
	case nil, SubStatusError, JsonError:
		return http.StatusOK
	case ErrNotFound:
		return http.StatusOK
	case ErrAlreadyExists, ErrInconsistentIDs:
		return http.StatusBadRequest
	case ErrCannotConnect:
		return 503
	case ErrSQL:
		return 504
	default:
		return http.StatusInternalServerError
	}
}

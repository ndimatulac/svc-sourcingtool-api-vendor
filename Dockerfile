FROM billyteves/alpine:3.5.0

MAINTAINER USAutopartsPOD01

RUN apk add --no-cache --virtual --update \
  tzdata

ADD ./bin/svc-sourcingtool-api-vendor /usr/local/bin/svc-sourcingtool-api-vendor

RUN chmod +x /usr/local/bin/svc-sourcingtool-api-vendor

ENTRYPOINT ["svc-sourcingtool-api-vendor"]

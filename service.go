package vendorsvc

import (
	"errors"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	"golang.org/x/net/context"

	_ "github.com/asaskevich/govalidator"
)

// longbow "bitbucket.org/usautoparts/svc-longbow-api"

type Service interface {
	GetVendors(ctx context.Context) error
}

type Vendor struct {
	VendorID   int
	VendorName string
}

var (
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrAlreadyExists   = errors.New("already exists")
	ErrNotFound        = errors.New("not found")
	ErrCannotConnect   = errors.New("Cannot connect to DB")
	ErrInput           = errors.New("Invalid input")
	ErrSQL             = errors.New("SQL Error")
	SubStatusError     = errors.New("Cannot update sub-status, check the rules on updating Sourcing tool status.")
	JsonError          = errors.New("JSON field `rfq_status_id` not found in request")
)

/** GORM **/
type vendorService struct {
	v Vendor
}

func NewVendorService() Service {
	return vendorService{
		v: Vendor{},
	}
}

func ConnectDB() *gorm.DB {
	session, err := gorm.Open(os.Getenv("SOURCING_VENDOR_GORM_DBTYPE"), os.Getenv("SOURCING_VENDOR_MYSQL_HOST"))
	if err != nil {
		fmt.Println(ErrCannotConnect, err.Error())
		return nil
	}
	return session
}

func (s vendorService) GetVendors(ctx context.Context) (err error) {
	db := ConnectDB()
	if db == nil {
		return ErrCannotConnect
	}
	defer db.Close()

	return nil
}
